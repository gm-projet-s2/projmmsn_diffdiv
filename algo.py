#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy
import math
import matplotlib.pyplot as plt


# évaluation du polynôme d'interpolation au point a
# beta est la liste des coefficients du polynôme dans la base de Newton
# xi est la liste des abscisses des points d'interpolation
# n le degré du polynôme
# a l'abscisse auquel on évalue le polynôme
def horner_newton(beta, xi, n, a):
    res = beta[n]
    for j in range(n-1, -1, -1):
        res = beta[j] + (a - xi[j])*res
    return res


# calcul des n+1 points d'interpolation optimaux entre a et b
def calculPointsInterpolation(a, b, n) :
    return [0.5 * (a + b) + (b - a) * math.cos(math.pi * (2 * i + 1)/(2 * (n + 1))) * 0.5 for i in range(n + 1)]

# calcul des coefficients du polynôme d'interpolation dans la base de Newton
# xi la liste des abscisses des points d'interpolation
# yi la liste des ordonnées des points d'interpolation
# n degré du polynôme d'interpolation
def differences_divisees(xi, yi, n):
    beta = numpy.copy(yi)
    for j in range(1, n+1):
        for i in range(n, j-1, -1):
            beta[i] = (beta[i] - beta[i-1]) / (xi[i] - xi[i-j])
    return beta



# calcul  de l'erreur maximale pour les points milieux des intervalles [xi ; x(i+1)]
# beta est la liste des coefficients du polynôme dans la base de Newton
# xi est la liste des abscisses des points d'interpolation
# n le degré du polynôme
# f la fonction interpolée
def calcul_erreur(beta, xi, n, f):
    maxErreur = 0.
    for i in range(n) :
        ai = (xi[i] + xi[i+1]) / 2.
        erreur = abs( f(ai) - horner_newton(beta, xi, n, ai) )
        # print("erreur" , i, " : ",erreur) # pour tests
        if (erreur > maxErreur) :
            maxErreur = erreur
    return maxErreur

# calcul  de l'erreur relative maximale pour les points milieux des intervalles [xi ; x(i+1)]
# beta est la liste des coefficients du polynôme dans la base de Newton
# xi est la liste des abscisses des points d'interpolation
# n le degré du polynôme
# f la fonction interpolée
def calcul_erreur_relative(beta, xi, n, f):
    maxErreur = 0.
    for i in range(n) :
        ai = (xi[i] + xi[i+1]) / 2.
        erreur = abs( (f(ai) - horner_newton(beta, xi, n, ai)) / f(ai) )
        # print("erreur" , i, " : ",erreur, "f(ai) =",f(ai)) # pour tests
        if (erreur > maxErreur) :
            maxErreur = erreur
    return maxErreur
