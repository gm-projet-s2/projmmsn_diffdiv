Projet MMSN de la deuxième vague du deuxième semestre
le but du projet est d'implémenter la méthode des différences divisées
Ce projet a été codé en python est peut être interprété avec python3
Il contient 3 fichiers de programmes principaux :
test.py : sert à vérifier le bon fonctionnement des fonctions de algo.py
main.py : pour les calculs en double précision (mise-à-jour : permet maintenant de tracer des courbes d'erreurs)
main_simple_prec.py : pour les calculs en simple précision  
