#!/usr/bin/env python
# -*- coding: utf-8 -*-

# programme testé avec l'interpreteur python3

import numpy as np
import math
import matplotlib.pyplot as plt
import algo_simple_prec as algo



# main
print ("choix de la fonction : ")
print ( "1 : cos(5x)e^(3x) sur [0 ; 2pi] ")
print("2 : cos(x) + cos(2x) + sin(3x) +sin(4x) sur [-2pi ; 2pi]")
print("3 : x^15 - 6 x^9 + 3x^4 + x + 2 sur [-1 ; 1]")
print("4 : 1/(1+x^2) sur [-3 ; 3]")
choix_fonction = int(input())

n = int(input("nombre de points d'interpolation : \n")) - 1  # -1 car n est le degré du polynôme et non le nombre de points

print("choix des points :")
print("1 : points de Chebychev")
print("2 : points equidistants")
print(" 3 : les deux ")
choix_points = int(input())

#choix de la fonction et de l'intervalle d'étude
if (choix_fonction == 1) :
    f = lambda x : np.float32(math.exp(3 * x) * math.cos(5 * x))
    a = np.float32(0)
    b = np.float32(2 * math.pi)
elif (choix_fonction == 2) :
    f = lambda x : np.float32(math.cos(x) + math.cos(2 * x) + math.sin(3 * x) + math.sin(4 * x))
    a = np.float32(- 2 * math.pi)
    b = np.float32(2 * math.pi)
elif (choix_fonction == 3):
    f = lambda x :  np.float32(x**15 - 6 * x**9 + 3 * x**4 + x + 2)
    a = np.float32(-1)
    b = np.float32(1)
else :
    f = lambda x : np.float32(1/(1 + x**2))
    a = np.float32(-3)
    b = np.float32(3)


x = np.linspace(a, b, 200)
y = [f(t) for t in x]
plt.plot(x, y, label="f(x)") #on trace la fonction f

if (choix_points != 2) : #erreurs et courbes pour les points de Chebychev
    xi = algo.calculPointsInterpolation(a, b, n)    # calcule des abscisses des points d'interpolations
    beta = algo.differences_divisees(xi, [f(x) for x in xi], n) # calcule du polynôme d'interpolation
    print("erreur avec les points de Chebychev : ", algo.calcul_erreur(beta, xi, n ,f))
    print("erreur relative avec les points de Chebychev: ", algo.calcul_erreur_relative(beta, xi, n ,f))
    y = [algo.horner_newton(beta, xi, n, t) for t in x]
    plt.plot(x, y, label="interpolation(points de Chebychev)") # courbe du polynôme

if (choix_points != 1) : # erreurs et courbes pour les points équidistants
    xiEqui = [np.float32(a + i * (b - a) / n) for i in range(n + 1)]    # calcule des abscisses des points d'interpolations
    betaEqui = algo.differences_divisees(xiEqui, [f(x) for x in xiEqui], n) # calcule du polynôme d'interpolation
    print("erreur avec les points equidistants : ", algo.calcul_erreur(betaEqui, xiEqui, n ,f))
    print("erreur relative avec les points equidistants : ", algo.calcul_erreur_relative(betaEqui, xiEqui, n ,f))
    y = [algo.horner_newton(betaEqui, xiEqui, n, t) for t in x]
    plt.plot(x, y, label="interpolation(points equidistants)") # courbe du polynôme
plt.legend(loc='upper left')
plt.show()
