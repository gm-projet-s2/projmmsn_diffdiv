#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy
import math
import algo


# programme pour tester les différentes fonctions du fichier algo.py

# test horner_newton
print("TEST horner_newton")
beta = numpy.array([1.,1.,1.])
n = 2
xi = numpy.zeros(n+1)
yi = numpy.zeros(n+1)
print("polynome 1+ x^2 :")
for i in range(0,n+1):
    xi[i] = i
    print("a = ", xi[i])
    yi[i] = algo.horner_newton(beta, xi, n, xi[i])
    print("p(a) = ", yi[i] )


# test calculPointsInterpolation
print("TEST calculPointsInterpolation")
print(algo.calculPointsInterpolation(-1, 1, 1))


# test differences_divisees
print("TEST differences_divisees")

beta2 = algo.differences_divisees(xi,yi,n)
print("valeurs attendues", beta)
print("valeurs obtenues", beta2)

#test calcul_erreur
print("TEST calcul_erreur")
# interpolation exacte
beta = numpy.array([1.,1.,1.])
n = 2
xi = numpy.array([0., 1. , 2.])
f = lambda x : x*x + 1
print ("interpolation exacte : ", algo.calcul_erreur(beta, xi, n ,f))

# interpolation avec une erreur
f = lambda x : math.cos(x)
a = 0
b = 2 * math.pi
n = 5
xi = algo.calculPointsInterpolation(a, b, n)
beta = algo.differences_divisees(xi, [f(x) for x in xi], n)
print ("interpolation non exacte : ", algo.calcul_erreur(beta, xi, n ,f))
