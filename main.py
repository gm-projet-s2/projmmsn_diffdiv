#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# programme testé avec l'interpreteur python3

import numpy
import math
import matplotlib.pyplot as plt
import algo



# main

print("choix du graphique : ")
print("1 : courbes interpolation")
print("2 : courbes d'erreur")
choix_graph = int(input())

print ("choix de la fonction : ")
print ( "1 : cos(5x)e^(3x) sur [0 ; 2pi] ")
print("2 : cos(x) + cos(2x) + sin(3x) +sin(4x) sur [-2pi ; 2pi]")
print("3 : x^15 - 6 x^9 + 3x^4 + x + 2 sur [-1 ; 1]")
print("4 : 1/(1+x^2) sur [-3 ; 3]")
choix_fonction = int(input())

if (choix_graph == 1) :
    n = int(input("nombre de points d'interpolation : \n")) - 1  # -1 car n est le degré du polynôme et non le nombre de points
else :
    n = int(input("nombre maximum de points d'interpolation : \n")) - 1

print("choix des points :")
print("1 : points de Chebychev")
print("2 : points equidistants")
print(" 3 : les deux ")
choix_points = int(input())

#choix de la fonction et de l'intervalle d'étude
if (choix_fonction == 1) :
    f = lambda x : math.exp(3 * x) * math.cos(5 * x)
    a = 0
    b = 2 * math.pi
elif (choix_fonction == 2) :
    f = lambda x : math.cos(x) + math.cos(2 * x) + math.sin(3 * x) + math.sin(4 * x)
    a = - 2 * math.pi
    b = 2 * math.pi
elif (choix_fonction == 3) :
    f = lambda x :  x**15 - 6 * x**9 + 3 * x**4 + x + 2
    a = -1
    b = 1
else :
    f = lambda x : 1/(1 + x**2)
    a = -3
    b = 3


if (choix_graph == 1) : #si l'utilisateur choisit de tracer les courbes d'interpolations
    x = numpy.linspace(a, b, 500)
    y = [f(t) for t in x]
    plt.plot(x, y, label="f(x)")    #on trace la fonction f

    if (choix_points != 2) : #erreurs et courbes pour les points de Chebychev
        xi = algo.calculPointsInterpolation(a, b, n)    # calcule des abscisses des points d'interpolations
        beta = algo.differences_divisees(xi, [f(x) for x in xi], n) # calcule du polynôme d'interpolation
        print("erreur avec les points de Chebychev : ", algo.calcul_erreur(beta, xi, n ,f))
        print("erreur relative avec les points de Chebychev : ", algo.calcul_erreur_relative(beta, xi, n ,f))
        y = [algo.horner_newton(beta, xi, n, t) for t in x]
        plt.plot(x, y, label="interpolation(points de Chebychev)") # courbe du polynôme

    if (choix_points != 1) : # erreurs et courbes pour les points équidistants
        xiEqui = [a + i * (b - a) / n for i in range(n + 1)]    # calcule des abscisses des points d'interpolations
        betaEqui = algo.differences_divisees(xiEqui, [f(x) for x in xiEqui], n)  # calcule du polynôme d'interpolation
        print("erreur avec les points equidistants : ", algo.calcul_erreur(betaEqui, xiEqui, n ,f))
        print("erreur relative avec les points equidistants : ", algo.calcul_erreur_relative(betaEqui, xiEqui, n ,f))
        y = [algo.horner_newton(betaEqui, xiEqui, n, t) for t in x]
        plt.plot(x, y, label="interpolation(points equidistants)") # courbe du polynôme
    plt.legend(loc='upper left')
    plt.show()

else : #si l'utilisateur choisit de tracer les courbes d'erreurs

    nlist = range (1,n+1)
    if (choix_points != 2) : #pour les points de Chebychev
        erreurOpti = []
        for nj in nlist : #on calcule les erreurs pour chaque nj dans [1;n]
            xi = algo.calculPointsInterpolation(a, b, nj)
            beta = algo.differences_divisees(xi, [f(x) for x in xi], nj)
            erreurOpti.append(algo.calcul_erreur(beta, xi, nj , f))
        plt.plot(nlist,erreurOpti, label="Points de Chebychev") # on trace la courbe d'erreur

    if (choix_points != 1) : #pour les points équidistants
        erreurEqui = []
        for nj in nlist : #on calcule les erreurs pour chaque nj dans [1;n]
            xiEqui = [a + i * (b - a) / nj for i in range(nj + 1)]
            betaEqui = algo.differences_divisees(xiEqui, [f(x) for x in xiEqui], nj)
            erreurEqui.append(algo.calcul_erreur(betaEqui, xiEqui, nj ,f))
        plt.plot(nlist,erreurEqui, label="Points Equidistants") # on trace la courbe d'erreur

    plt.legend(loc='upper left')
    plt.ylabel("erreur")
    plt.xlabel("degré du polynôme d'interpolation")
    plt.show()
